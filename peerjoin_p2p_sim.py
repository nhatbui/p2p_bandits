from __future__ import division
from datetime import datetime
from pprint import PrettyPrinter
from collections import defaultdict
import numpy as np
import math
import itertools
import matplotlib.pyplot as plt
from p2p_bandits.peer import Peer


# Simulates one group of 4 peers
def createPeers(res, sys_min):
    count = 1
    return [
        Peer(
            pid,
            bps,
            sys_min,
            {p: (sys_min, count) for p in res.iterkeys() if p is not pid}
        )
        for pid, bps in res.iteritems()
    ]

# Max Bandwidth (Mbps)
max_bps = 30
# Min Bandwidth (Mbps)
min_bps = 5

# Number of peers in group
n_peers = 4

# Randomized Maximum Bandwidth per Peer (Mbps)
# (arms per peer)
L = {0: 28, 1: 18, 2:15, 3: 23}
print 'Maximum Bandwidth of Peers'
for k, v in L.iteritems():
    print 'Bandit {}: {}'.format(k, v)
group_L = sum(L.values())

# Peer minimum contribution of policy
# a.k.a., each must allocate at least this amount to be receive
# resources in return.
# "Buy-In"
min_contrib = 7

# Rounds
T = 100

# New peer will join at...
t_join = int(T/2) + 1
# Max upload rate of new joining peer.
res_join = 10

# Simulations
simulations = 1

# Variables to Report Progress of Simulations
if simulations == 1:
    prg = 1
else:
    prg = simulations/10

# Stats to Track
avg_cumulative_rewards = None
avg_rewards = None

# Utility
pp = PrettyPrinter()

# Start ith simulation
for sim in xrange(simulations):
    # Print status
    if sim % prg == 0:
        print '{}% {}'.format(
            sim*100/simulations,
            datetime.now().strftime('%m/%d %H:%M')
        )

    # Create peers
    # Initialize all peers to expect all other peers
    # can at least provide the minimum contribution.
    peers = createPeers(L, min_contrib)
    assert(len(peers) == n_peers)
    for peer in peers:
        assert(peer.name not in peer.est_alloc)
        assert(len(peer.est_alloc) == (n_peers - 1))

    # Initialize States
    S = {pid: {} for pid in L.iterkeys()}

    # Stats
    # Account now for joining peer.
    rewards = np.zeros((n_peers+1, T))
    cumulative_rewards = np.zeros((n_peers+1, T))

    # Run one simulation
    for t in xrange(T):
        if t == t_join:
            # Add new arm to all peers
            for peer in peers:
                peer.est_alloc[n_peers] = (min_contrib, 1)

            peers.append(
                Peer(
                    n_peers,
                    res_join,
                    min_contrib,
                    {p: (min_contrib, 1) for p in L.iterkeys()}
                )
            )
            n_peers += 1
            group_L += res_join

            print 'State Before Peer Joining'
            pp.pprint(S)

        # new states for round t
        S_new = defaultdict(lambda: {})
        arms = {}
        features = {}
        for peer in peers:
            if peer.name in S:
                s = S[peer.name]
            else:
                # If peer doesn't have any states,
                # it means they aren't receiveing any resources.
                s = {}

            arm, res_alloc, feature = peer.choose_res_alloc(s, min_contrib)
            assert(arm in L.keys() or arm == 'no')

            # Map the resource allocations as the new states.
            for peer_k, r in res_alloc.iteritems():
                S_new[peer_k][peer.name] = r

            # Save these values for updating
            arms[peer.name] = arm
            features[peer.name] = feature

        # Compute Rewards
        # We create an array for it because we keep a rewards
        # history as a matrix.
        r = []
        # For each peer...
        for peer in peers:
            # ... it's reward is the sum of allocation from peers.
            r.append(sum(S_new[peer.name].values()))
        # Add reward for joining peer.
        if t < t_join:
            r.append(0)
        r = np.array(r)
        rewards[:, t] = r
        assert(int(sum(r)) <= group_L)

        # Update all peers with their rewards
        for i, peer in enumerate(peers):
            peer.update(arms[peer.name], features[peer.name], r[i])

        # Save stats
        if t == 0:
            cumulative_rewards[:, t] = r
        else:
            cumulative_rewards[:, t] = cumulative_rewards[:, t-1] + r

        # Copy the new state into this variable for the next time step
        S = S_new

    # Simulation Statistics
    if sim == 0:
        avg_cumulative_rewards = cumulative_rewards
        avg_rewards = rewards
    else:
        avg_cumulative_rewards = np.dstack((avg_cumulative_rewards, cumulative_rewards))
        avg_cumulative_rewards = np.average(avg_cumulative_rewards, axis=2, weights=[float(sim-1)/sim, 1./sim])

        avg_rewards = np.dstack((avg_rewards, rewards))
        avg_rewards = np.average(avg_rewards, axis=2, weights=[float(sim-1)/sim, 1./sim])

print 'Steady-State Resource Allocation'
pp.pprint(S)

# Joined Peer Plot
join_plt, ax = plt.subplots(1, 1) # Cumulative rewards plot
ax.plot(range(T), avg_rewards[n_peers-1, :])
# Remove the plot frame lines. They are unnecessary chartjunk.
ax.spines["top"].set_visible(False)    
ax.spines["bottom"].set_visible(False)    
ax.spines["right"].set_visible(False)    
ax.spines["left"].set_visible(False)

# Ensure that the axis ticks only show up on the bottom and left of the plot.    
# Ticks on the right and top of the plot are generally unnecessary chartjunk.    
ax.get_xaxis().tick_bottom()
ax.get_yaxis().tick_left()

# Provide tick lines across the plot to help your viewers trace along    
# the axis ticks. Make sure that the lines are light and small so they    
# don't obscure the primary data lines.    
for y in range(0, 61, 10):    
    ax.plot(range(T), [y] * len(range(T)), "--", lw=0.5, color="black", alpha=0.3)
    
# Remove the tick marks; they are unnecessary with the tick lines we just plotted.    
ax.tick_params(axis="both", which="both", top="off",    
               labelbottom="on", left="off", right="off", labelleft="on", labelsize=8) 
ax.set_title('Peer {}'.format(i), fontsize=9)

# Instantaneous Rewards Plot
r_plt, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2,sharex=True, sharey=True) # Cumulative rewards plot
r_ax = [ax1, ax2, ax3, ax4]

# Make room for the title then add it
r_plt.subplots_adjust(top=.86)
r_plt.suptitle('Instantaneous Rewards', fontsize=14, ha='center')
# Manually set axes labels
r_plt.text(0.3, 0.035, 'Round', fontsize=9, ha='center')
r_plt.text(0.72, 0.035, 'Round', fontsize=9, ha='center')
r_plt.text(0.045, 0.35, 'Reward', fontsize=9, ha='center', rotation='vertical')
r_plt.text(0.045, 0.77, 'Reward', fontsize=9, ha='center', rotation='vertical')

for i, ax in enumerate(r_ax):
    ax.plot(range(T), avg_rewards[i, :])
    
    # Remove the plot frame lines. They are unnecessary chartjunk.
    ax.spines["top"].set_visible(False)    
    ax.spines["bottom"].set_visible(False)    
    ax.spines["right"].set_visible(False)    
    ax.spines["left"].set_visible(False)
    
    # Ensure that the axis ticks only show up on the bottom and left of the plot.    
    # Ticks on the right and top of the plot are generally unnecessary chartjunk.    
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    
    # Provide tick lines across the plot to help your viewers trace along    
    # the axis ticks. Make sure that the lines are light and small so they    
    # don't obscure the primary data lines.    
    for y in range(0, 61, 10):    
        ax.plot(range(T), [y] * len(range(T)), "--", lw=0.5, color="black", alpha=0.3)
        
    # Remove the tick marks; they are unnecessary with the tick lines we just plotted.    
    ax.tick_params(axis="both", which="both", top="off",    
                   labelbottom="on", left="off", right="off", labelleft="on", labelsize=8) 
    ax.set_title('Peer {}'.format(i), fontsize=9)

join_plt.savefig('img/joined_bandit.png', dpi=300)
r_plt.savefig('img/join_bandits_rewards.png', dpi=300)
