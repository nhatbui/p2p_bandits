// Size of SVG
var width = 960,
    height = 500;

// Connect to Firebase
var firebaseRef = new Firebase("https://p2pbandits.firebaseio.com");

// Color pallette for node groups
var color = d3.scale.category20();

// handles to links and nodes element groups
var links = [];
var nodes = [];

// Force
var force = d3.layout.force()
    .nodes(nodes)
    .links(links)
    .charge(-60)
    .linkDistance(30)
    .gravity(.05)
    .size([width, height])
    .on("tick", tick);

// Add SVG element to body
var svg = d3.select("body").append("svg")
    .attr("width", width)
    .attr("height", height);
var link = svg.selectAll(".link");
var node = svg.selectAll(".node");

// Create Network
// With the Firebase Reference,
// we attach an asynchronous listener to 
// listen to changes to the value
// (hence .on("value",...)) of the key: 
// p2pbandits.firebase.io/demo
firebaseRef.child("sim").on("value", function(graphSnapshot) {
  // Unpack the Firebase object to retrieve the JSON object.
  var graph = graphSnapshot.val();
  if (graph == null) {
    return;
  }

  if ("nodes" in graph && nodes.length == 0) {
    nodes.push.apply(nodes, graph.nodes);
    node = svg.selectAll(".node").data(force.nodes())
      .enter().append("circle")
        .attr("class", "node")
        .attr("r", 5)
        .style("fill", function(d) { return color(d.group); })
        .call(force.drag);
  }

  if ("links" in graph) {
    force.links(graph.links)
    link = svg.selectAll(".link").data(graph.links);
    link.enter().append("line")
        .attr("class", "link")
        .style("stroke-width", function(d) { return Math.sqrt(d.value); });
    link.exit().remove();
  }

  // Join new data with old elements, if any.
  //link = svg.selectAll(".link").data(force.links());
  //node = svg.selectAll(".node").data(force.nodes());

  // Remove old elements as needed.
  //link.exit().remove();
  //node.exit().remove();  

  force.start();
}, function (errorObject) {
  console.log("The read failed: " + errorObject.code);
});

function tick() {
      link.attr("x1", function(d) { return d.source.x; })
          .attr("y1", function(d) { return d.source.y; })
          .attr("x2", function(d) { return d.target.x; })
          .attr("y2", function(d) { return d.target.y; });

      node.attr("cx", function(d) { return d.x; })
          .attr("cy", function(d) { return d.y; });
    }
