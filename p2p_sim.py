# coding: utf-8
from __future__ import division
from networkx.readwrite import json_graph
from datetime import datetime
from collections import defaultdict
from pprint import PrettyPrinter
from firebase import firebase
from networkx.readwrite import json_graph
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import networkx as nx
import time
import argparse
import json
from p2p_bandits.peer import Peer


# Simulates a group of n peers

# Arguments
ap = argparse.ArgumentParser()
ap.add_argument('-n', '--numPeers', help='Number of Peers in Sim', type=int, default=4)
ap.add_argument('-t', '--tRounds', help='Number of rounds per Sim', type=int, default=50)
ap.add_argument('-s', '--sims', help='Number of Simulations', type=int, default=1)
ap.add_argument('--maxbps', help='Max. upload bandwidth of any peer', type=int, default=30)
ap.add_argument('--minbps', help='Min. upload bandwidth of any peer', type=int, default=5)
ap.add_argument('-c', '--contribution', help='Min. upload bandwidth to join system', type=int, default=7)
ap.add_argument('-a', '--animate', help='Option to animate simulation. Sends states to Firebase.', action='store_true')
args = ap.parse_args()

# Max Bandwidth (Mbps)
max_bps = args.maxbps

# Min Bandwidth (Mbps)
min_bps = args.minbps

# Number of peers in group
n_peers = args.numPeers

# Peer minimum contribution of policy
# a.k.a., each must allocate at least this amount to be receive
# resources in return.
# "Buy-In"
min_contrib = args.contribution

# Rounds
T = args.tRounds

# Simulations
simulations = args.sims

# Randomized Maximum Bandwidth per Peer (Mbps)
# (arms per peer)
peerIDs = range(n_peers)
L = {i: np.random.randint(min_bps, max_bps) for i in xrange(n_peers)}
print 'Maximum Bandwidth of Peers'
for k, v in L.iteritems():
    print 'Bandit {}: {}'.format(k, v)
group_L = sum(L.values())

S_df = pd.DataFrame(np.zeros((n_peers, n_peers)), index=peerIDs, columns=peerIDs)
allocation_network = nx.DiGraph(S_df.as_matrix())

if args.animate:
    db_url = 'https://p2pbandits.firebaseio.com'
    db_node = 'sim'
    firebase = firebase.FirebaseApplication('{}/{}'.format(db_url, db_node), None)

    # Delete any old data
    firebase.delete(db_node, None)

    response = firebase.patch(db_node,
                             data=json_graph.node_link_data(allocation_network))
    patch_url = '{}/{}'.format(db_url, db_node)

# Variables to Report Progress of Simulations
if simulations == 1:
    prg = 1
else:
    prg = simulations/10

# Stats to Track
avg_cumulative_rewards = None
avg_rewards = None

# Start ith simulation
for sim in xrange(simulations):
    if sim % prg == 0:
        print '{}% {}'.format(sim*100/simulations,
                              datetime.now().strftime('%m/%d %H:%M'))

    # Create peers
    # Initialize all peers to expect all other peers
    # can at least provide the minimum contribution.
    peers = [
        Peer(
            pid,
            bps,
            min_contrib,
            {p: (min_contrib, 1) for p in L.iterkeys() if p is not pid}
        )
        for pid, bps in L.iteritems()
    ]
    assert(len(peers) == n_peers)
    for peer in peers:
        assert(peer.name not in peer.est_alloc)
        assert(len(peer.est_alloc) == (n_peers - 1))

    # Initialize States
    # cold-start
    S = {pid: {} for pid in L.iterkeys()}

    # Stats
    rewards = np.zeros((n_peers, T))
    cumulative_rewards = np.zeros((n_peers, T))

    # Run one simulation
    for t in xrange(T):
        # new states for round t
        S_new = defaultdict(lambda: {})
        # arms
        arms = {}
        # features
        features = {}
        for peer in peers:
            #print t, S
            if peer.name in S:
                s = S[peer.name]
            else:
                # If peer doesn't have any states,
                # it means they aren't receiveing any resources.
                s = {}

            arm, res_alloc, feature = peer.choose_res_alloc(s, min_contrib)
            assert(arm in L.keys() or arm == 'no')

            # Map the resource allocations as the new states.
            for peer_k, r in res_alloc.iteritems():
                S_new[peer_k][peer.name] = r

            # Save these values for updating
            arms[peer.name] = arm
            features[peer.name] = feature

        # Rewards
        # We create an array for it because we keep a rewards
        # history as a matrix.
        r = []
        # For each peer...
        for peer in peers:
            # ...it's reward is the sum of allocation from peers.
            r.append(sum(S_new[peer.name].values()))
        rewards[:, t] = r
        assert(int(sum(r)) <= group_L)

        # Save Rewards
        if t == 0:
            cumulative_rewards[:, t] = r
        else:
            cumulative_rewards[:, t] = cumulative_rewards[:, t-1] + r

        # Update performance of arms
        for i, peer in enumerate(peers):
            peer.update(arms[peer.name], features[peer.name], r[i])
        
        # Create adjacency matrix for resource reciprocation
        if args.animate:
            S_df = pd.DataFrame(np.zeros((n_peers, n_peers)), index=peerIDs, columns=peerIDs)
            for peer_i, alloc in S.iteritems():
                for peer_j, res in alloc.iteritems():
                    S_df.at[peer_j, peer_i] = res
            allocation_network = nx.DiGraph(S_df.as_matrix())

            firebase.patch(patch_url,
                           data=json_graph.node_link_data(allocation_network),
                           params={'print': 'silent'})
            time.sleep(.25)

        # Create new state
        S = S_new

    if sim == 0:
        avg_cumulative_rewards = cumulative_rewards
        avg_rewards = rewards
    else:
        avg_cumulative_rewards = np.dstack((avg_cumulative_rewards,
                                            cumulative_rewards))
        avg_cumulative_rewards = np.average(avg_cumulative_rewards,
                                            axis=2,
                                            weights=[float(sim-1)/sim, 1./sim])

        avg_rewards = np.dstack((avg_rewards, rewards))
        avg_rewards = np.average(avg_rewards,
                                 axis=2,
                                 weights=[float(sim-1)/sim, 1./sim])

pp = PrettyPrinter()
pp.pprint(S)

# Let's go through and color code groups.
# before saving the JSON
with open('img/plt_this_network.json', 'wb') as f:
    network_dict = json_graph.node_link_data(allocation_network)
    for peer, reciprocating_peers in S.iteritems():
        if len(reciprocating_peers.keys()) > 1:
            network_dict["nodes"][peer]["group"] = 1
        elif len(reciprocating_peers.keys()) == 1:
            network_dict["nodes"][peer]["group"] = 2
        else:
            network_dict["nodes"][peer]["group"] = 3
    json.dump(network_dict, f)
