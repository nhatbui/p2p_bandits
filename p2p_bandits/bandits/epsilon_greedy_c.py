import numpy as np
import random
import pdb

class ContextualEpsilonGreedy:
	def __init__(self, epsilon, n_arms, n_policies):
		self.epsilon = epsilon
		self.values = [0.0 for p in xrange(n_policies)]
		self.counts = [0 for p in xrange(n_policies)]
		self.n_arms = n_arms

	def select_policy(self):
		if random.random() > self.epsilon:
			return np.argmax(self.values), 1.0
		else:
			return random.randrange(len(self.values)), 1.0

	def update(self, chosen_policy, reward, probability):
		self.counts[chosen_policy] += 1
		n = self.counts[chosen_policy]

		value = self.values[chosen_policy]
		new_value = ((n - 1) / float(n)) * value + \
					(1 / float(n)) * reward/probability
		self.values[chosen_policy] = new_value
