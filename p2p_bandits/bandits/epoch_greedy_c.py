import numpy as np
import random
import math
import pdb


def categorical_draw(probs):
    z = random.random()
    cumulative_prob = 0.0
    for i in xrange(len(probs)):
        prob = probs[i]
        cumulative_prob += prob
        if cumulative_prob > z:
            return i, 1.0
    return len(probs) - 1, 1.0

class ContextualEpochGreedy:
    def __init__(self, temperature, n_policies):
        self.temperature = temperature
        self.values = [0.0 for p in xrange(n_policies)]
        self.counts = [0 for p in xrange(n_policies)]

    def select_policy(self):
        t = sum(self.counts) + 1
        temperature = 1 / math.log(t + 0.0000001)

        z = sum([math.exp(v / temperature) for v in self.values])
        probs = [math.exp(v / temperature) / z for v in self.values]
        return categorical_draw(probs)

    def update(self, chosen_policy, reward, probability):
        self.counts[chosen_policy] += 1
        n = self.counts[chosen_policy]

        value = self.values[chosen_policy]
        new_value = ((n - 1) / float(n)) * value + \
                    (1 / float(n)) * reward/probability
        self.values[chosen_policy] = new_value
