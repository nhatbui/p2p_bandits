import math
from scipy import stats
import numpy as np
import random
import matplotlib.pyplot as plt
from datetime import datetime
from bernoulli_arm import BernoulliArm as ba


class FollowTheLeader():
	def __init__(self, tau, counts, values):
		self.counts = counts
		self.values = values
		self.tau = tau
		self.count = 0

	def initialize(self, n_arms):
		self.counts = [0 for _ in xrange(n_arms)]
		self.values = [0.0 for _ in xrange(n_arms)]
		self.count = 0

	def select_arm(self):
		if self.count < self.tau:
			return random.randrange(len(self.counts))
		else:
			return np.argmax(self.values)

	def update(self, chosen_arm, reward):
		self.count += 1
		self.counts[chosen_arm] = self.counts[chosen_arm] + 1
		n = self.counts[chosen_arm]

		value = self.values[chosen_arm]
		new_value = ((n - 1) / float(n)) * value + (1 / float(n)) * reward
		self.values[chosen_arm] = new_value

if __name__ == '__main__':
	# Actions
	n_arms = 3

	# Rewards
	rewards = [ba(random.random()) for arm in xrange(n_arms)]

	# Time Horizon
	T = 10000

	# Exploring Rounds
	tau = math.pow(T, 1./3)
	ftl = FollowTheLeader(
			tau,
			[0 for _ in xrange(n_arms)],
			[0.0 for _ in xrange(n_arms)]
		)

	# Distribution of Context and Rewards
	#n_contexts = 4
	#D = [[np.random.random() for _ in xrange(len(A))] for _ in xrange(n_contexts)]
	#print D

	# Best Policy
	#best = [0 for _ in xrange(n_contexts)]
	#for context in xrange(n_contexts):
	#	best[context] = np.argmax(D[context])
	#print 'Best Policy', best

	# Cumulative Rewards
	cumulative_reward = [0.0 for _ in xrange(T)]

	# Regret
	regret = [0.0 for _ in xrange(T)]

	# Log Vector
	#h = np.zeros((n_contexts, len(A)))
	for t in xrange(T):
		if t % prog == 0:
			print t*100/T, '%', datetime.now()

		#context = np.random.randint(0, n_contexts)

		# Our FTL Bandit
		arm = ftl.select_arm()

		#reward = D[context][action]
		reward = rewards[arm].draw()
		#h[context][action] += reward

		if t == 0:
			cumulative_reward[t] = 0.0
		else:
			cumulative_reward[t] = cumulative_reward[t-1] + reward

		# Best Bandit
		#best_action = best[context]

		#best_reward = D[context][best_action]

		#if t == 0:
		#	regret[t] = best_reward - reward
		#else:
		#	regret[t] = regret[t-1] + (best_reward - reward)


	plt.figure(1)
	plt.plot(range(T), cumulative_reward)
	plt.vlines(int(tau), 0, cumulative_reward[-1])
	plt.title('Cumulative Reward')

	#plt.figure(2)
	#plt.plot(range(T), regret)
	#plt.title('Regret')

	plt.show()

