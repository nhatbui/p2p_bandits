from numpy import eye, zeros, sqrt, array
from numpy.linalg import inv
import random
import math
import pdb


class LinUCB:
    def __init__(self, alpha=1.0):
        self.alpha = alpha
        self.A = {}
        self.b = {}

    def select_arm(self, arms):
        """
        Arms is a dict.
        The arm's name is the key.
        Each value is a list of features for that arm.
        """
        p_max = 0
        max_arm = None
        for k_arm, features in arms.iteritems():
            features = array([features])
            if k_arm not in self.A:
                d = features.shape[1]
                self.A[k_arm] = eye(d)
                self.b[k_arm] = zeros((d, 1))
            theta = inv(self.A[k_arm]).dot(self.b[k_arm])
            p = theta.T.dot(features.T) + self.alpha*sqrt(features.dot(inv(self.A[k_arm])).dot(features.T))
            if p > p_max:
                p_max = p
                max_arm = k_arm
        return max_arm

    def update(self, chosen_arm, features, reward):
        """
        Chosen_arm is a key for self.A
        Features should be the features of chosen_arm
        Reward is a real-value scalar payoff.
        """
        features = array([features])
        self.A[chosen_arm] = self.A[chosen_arm] + features.T.dot(features)
        self.b[chosen_arm] = self.b[chosen_arm] + reward*features.T
