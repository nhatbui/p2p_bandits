from __future__ import division
from numpy import array, mean, matrix

from bandits.linucb import LinUCB

class Peer:
    def __init__(self, name, upload_bps, min_bps, peers, alpha=1.0):
        """
        upload_bps is the maximum upload bandwidth this peer can offer.
        peers must be a dict with peer ID as keys and estimated bandwidth
            contribution as the value.
        """
        self.name = name
        self.upload_bps = upload_bps
        self.min_bps = min_bps
        self.bandit = LinUCB(alpha)
        # TODO: use collection's default_dict
        self.est_alloc = peers

    def update(self, chosen_arm, features, reward):
        self.bandit.update(chosen_arm, features, reward)

    def compute_rr(self, states):
        """
        Returns dict representing resource allocation using a tit-for-tat policy.
        """

        # Do not consider any peers whose contributions
        # are less than the min. req.
        states = {peer: res for peer, res in states.iteritems() if res >= self.min_bps}

        # Compute tit-for-tat resource reciprocation.
        res_sum = sum(states.values())
        res_alloc = {peer: res*self.upload_bps/res_sum for peer, res in states.iteritems()}

        # Find peer with lowest contrib below the min. req. and
        # zero that contribution.
        min_res = self.min_bps
        pop_peer = None
        for peer, res in res_alloc.iteritems():
            if res < min_res:
                min_res = res
                pop_peer = peer
        if pop_peer:
            # We can't handle this peer. Remove them from the state
            # so we don't consider giving them resources.
            states.pop(pop_peer)
            res_alloc = self.compute_rr(states)
        return res_alloc


    def compute_features(self, new_peer, peers):
        """
        Returns array of length 2 that contains:
            1. allocation to kth peer
            2. est. resource allocation from peer k
        """
        v, i = self.est_alloc[new_peer]
        est_contrib = v/i

        test_peers = {k: v for k,v in peers.iteritems()}  # Make copy.
        test_peers[new_peer] = est_contrib  # Add new peer.
        # Calc resource reciprocation including new peer.
        rr = self.compute_rr(test_peers)

        # If we can't give the min. req. bps, don't give
        # any at all.
        if new_peer not in rr or rr[new_peer] < self.min_bps:
            rr[new_peer] = 0.0

        return [rr[new_peer], est_contrib]

    def choose_res_alloc(self, state, min_bps):
        """
        State is the resource received from other peers.
            It is a list of tuples where each tuple is
            (peer, resource)
        min_bps is the min. required bandwidth (in bps)
            required to give each peer.
        Returns states
        """
        if len(state) > 0:
            avg_rr = mean(state.values())
        else:
            avg_rr = 0

        # Record allocation of these peers so we know we have
        # an estimation of their potential resource allocation.
        # Currently, we use an average.
        # Also let's grab the IDs while we're at it.
        for peer, res in state.iteritems():
            if peer not in self.est_alloc:
                self.est_alloc[peer] = (res, 1)
            else:
                tot_bps, count = self.est_alloc[peer]
                self.est_alloc[peer] = (tot_bps+res, count+1)

        # Self-imposed Rule:
        # We are obliged to reciprocate a.k.a. we give
        # resources to all peers who have given us resources.

        # Now let's compute all our possible arms.
        # Arm 1. With a neutral attitude, reciprocate only peers
        #   who have allocated us resources.
        # Arm 2. With a neutral attitude, reciprocate with peers
        #   who have allocated us resources PLUS peer j where j: (0, k), j != i
        #   We use their estimated possible resource allocation.
        # We also must factor in how must resource we have to give.
        # It wouldn't be worthwhile if we partition are resources such that
        # we would receive an unsubstantial reciprocation.

        # For each peer we DID NOT get resources from,
        # compute how much we would reciprocate to them considering
        # how much we estimate they will give us.
        # compute_features will zero out if:
        # 1) peer i cannot give the req min. to peer k
        # 2) peer i will consider a contribution as 0 from peer k if it's less
        #    than the req. min.
        features = {peer: self.compute_features(peer, state)
                    for peer in self.est_alloc
                    if peer not in state}

        # Create the default arm.
        if len(state) > 0:
            # Default contribution: reciprocate resources to those that gave us some.
            default_contrib = self.compute_rr(state)
            # Add the "don't add new peer" arm
            features['no'] = array([mean([default_contrib.values()]), avg_rr])
        else:
            # Default contribution is nothing because we have recieved nothing.
            default_contrib = {}
            # Add the "don't add new peer" arm
            features['no'] = array([0, avg_rr])

        # Select an arm using LinUCB
        arm = self.bandit.select_arm(features)

        if arm == 'no':
            return 'no', default_contrib, features['no']
        else:
            state[arm] = features[arm][0]
            return arm, self.compute_rr(state), features[arm]
